#!/usr/bin/env bash

JAVAFILES=$(find src/main/java -type f -name '*.java' | tr '\n' ' ')
PACKAGES_ROOT="fr.univlille.iut.info.webutils"
PACKAGES="$PACKAGES_ROOT.model.data $PACKAGES_ROOT.view"
rm -rf doc/*
mkdir -p doc/dist
javadoc -sourcepath src/main/java -d doc -docletpath lib/umldoclet-2.1.2.jar -doclet nl.talsmasoftware.umldoclet.UMLDoclet $PACKAGES
javac -g -d target $JAVAFILES && jar cfv doc/dist/univlille-iutinfo-javaweb-utils.jar doc -C target .