package fr.univlille.iut.info.webutils.model.data;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

abstract class DAOBasic<T>
{
    private static final Map<Class, Method> sqlGetters;
    static
    {
        try
        {
            sqlGetters = Map.of(
                    String.class, ResultSet.class.getDeclaredMethod("getString", String.class)
            );
        } catch (NoSuchMethodException e)
        {
            throw new RuntimeException(e);
        }
    }

    private Class<T> pojoClass;
    private Field[] pojoAttributes;
    private Constructor<T> pojoConstructor;
    private List<JDBCReflection> jdbcReflectionsMethods;
    private Object[] pojoConstructorParams;
    private String tableName;

    public DAOBasic(Class<T> pojoClass) throws NoSuchMethodException
    {
        this.pojoClass = pojoClass;
        this.pojoAttributes = pojoClass.getDeclaredFields();
        this.pojoConstructor = pojoClass.getConstructor(pojoClass.getConstructors()[0].getParameterTypes());
        this.jdbcReflectionsMethods = new ArrayList<JDBCReflection>(pojoClass.getDeclaredFields().length);
        this.pojoConstructorParams = new Object[pojoClass.getDeclaredFields().length];
        for (Field attribute : this.pojoAttributes)
        {
            this.jdbcReflectionsMethods.add(new JDBCReflection(DAOBasic.sqlGetters.get(attribute.getType()), attribute.getName()));
        }
        this.tableName = pojoClass.getSimpleName().toLowerCase();
    }

    public List<T> findAll() throws SQLException, IOException, ClassNotFoundException
    {
        String sqlRequest = "SELECT * FROM " + this.tableName;
        DBLoop<List<T>> loop = new DBLoop<List<T>>(new ArrayList<T>(), sqlRequest, (((list, resultSet) ->
        {
            for (int i = 0; i < this.pojoAttributes.length; i++)
            {
                try
                {
                    this.pojoConstructorParams[i] = this.jdbcReflectionsMethods.get(i).execute(resultSet);
                }
                catch (InvocationTargetException | IllegalAccessException e)
                {
                    throw new RuntimeException(e);
                }
            }
            try
            {
                list.add(this.pojoClass.cast(this.pojoConstructor.newInstance(this.pojoConstructorParams)));
            }
            catch (InstantiationException | IllegalAccessException | InvocationTargetException e)
            {
                throw new RuntimeException(e);
            }
        })));
        return loop.executeLoop();
    }
}