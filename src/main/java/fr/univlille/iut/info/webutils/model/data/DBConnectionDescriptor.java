package fr.univlille.iut.info.webutils.model.data;

/**
 * Cette classe décrit une connection à une base de données (driver, url, données de connexion)
 */
public class DBConnectionDescriptor
{
    private String driver;
    private String url;
    private String login;
    private String password;

    /**
     * Constructeur
     * @param driver le nom du driver à utiliser
     * @param url l'url à utiliser pour la connexion à la base de données
     * @param login le nom de l'utilisateur de la base de données
     * @param password le mot de passe de l'utilisateur
     */
    public DBConnectionDescriptor(String driver, String url, String login, String password)
    {
        this.driver = driver;
        this.url = url;
        this.login = login;
        this.password = password;
    }

    /**
     * Constructeur
     * @param url l'url à utiliser pour la connexion à la base de données
     * @param login le nom de l'utilisateur de la base de données
     * @param password le mot de passe de l'utilisateur
     */
    public DBConnectionDescriptor(String url, String login, String password)
    {
        this(null, url, login, password);
    }

    /**
     * Constructeur
     * @param driver le nom du driver à utiliser
     * @param url l'url à utiliser pour la connexion à la base de données
     */
    public DBConnectionDescriptor(String driver, String url)
    {
        this(driver, url, null, null);
    }

    /**
     * Constructeur
     * @param url l'url à utiliser pour la connexion à la base de données
     */
    public DBConnectionDescriptor(String url)
    {
        this(null, url, null, null);
    }

    public String getDriver()
    {
        return driver;
    }

    public String getUrl()
    {
        return url;
    }

    public String getLogin()
    {
        return login;
    }

    public String getPassword()
    {
        return password;
    }
}
