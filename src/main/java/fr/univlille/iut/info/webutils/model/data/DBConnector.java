package fr.univlille.iut.info.webutils.model.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


/**
 * Cette classe gère la connexion avec une base de données
 */
public class DBConnector
{
    /**
     * L'ensemble des connexions actuellement configurées
     * Chaque connexion possède un url, un login, et un mot de passe
     */
    protected static Map<String, DBConnectionDescriptor> connectionDescriptorList;

    /**
     * Le nom de la connexion par défaut à utiliser (la 1ère à être configurée)
     */
    protected static String defaultConnectionName;

    /**
     * Cette méthode permet de déterminer si un driver peut se connecter à l'url.
     * @param url, l'url à utiliser pour la connexion à la base de données.
     */
    private static boolean availableDriver(String url) throws SQLException
    {
        Enumeration<Driver> driverList = DriverManager.getDrivers();
        boolean available = false;
        while (!available && driverList.hasMoreElements())
        {
            available = driverList.nextElement().acceptsURL(url);
        }
        return available;
    }

    /**
     * Cette méthode initialise la connexion à la base de données.
     * @param connectionName, le nom de la connexion
     * @param connectionDescriptor les informations de connexion à utiliser
     * @throws ClassNotFoundException lorsque le driver est introuvable
     * @throws SQLException lors d'une erreur de requête SQL.
     */
    public static void init(String connectionName, DBConnectionDescriptor connectionDescriptor) throws ClassNotFoundException, SQLException
    {
        if (connectionDescriptor.getDriver() != null)
        {
            Class.forName(connectionDescriptor.getDriver());
        }

        if (!availableDriver(connectionDescriptor.getUrl()))
        {
            throw new ClassNotFoundException("No driver available for the given url");
        }

        if (DBConnector.connectionDescriptorList == null)
        {
            if (connectionName == null)
            {
                connectionName = "default";
            }
            DBConnector.connectionDescriptorList = new HashMap<String, DBConnectionDescriptor>();
            DBConnector.defaultConnectionName = connectionName;
        }
        else
        {
            if (connectionName == null)
            {
                connectionName = "" + DBConnector.connectionDescriptorList.size();
            }
        }
        DBConnector.connectionDescriptorList.put(connectionName, connectionDescriptor);
    }

    /**
     * Cette méthode initialise la connexion à la base de données.
     * @param connectionDescriptor les informations de connexion à utiliser
     * @throws ClassNotFoundException lorsque le driver est introuvable
     * @throws SQLException lors d'une erreur de requête SQL.
     */
    public static void init(DBConnectionDescriptor connectionDescriptor) throws ClassNotFoundException, SQLException
    {
        init(null, connectionDescriptor);
    }

    /**
     * Cette méthode initialise la connexion à la base de données.
     * @param prop la liste des propriétées utiles à la connexion (name, driver, url, login, password)
     * @throws ClassNotFoundException lorsque le driver est introuvable
     * @throws SQLException lors d'une erreur de requête SQL.
     */
    public static void init(Properties prop) throws ClassNotFoundException, SQLException
    {
        init(prop.getProperty("name"), new DBConnectionDescriptor(prop.getProperty("driver"), prop.getProperty("url"), prop.getProperty("login"), prop.getProperty("password")));
    }

    /**
     * Cette méthode initialise la connexion à la base de données à partir du fichier de config spécifié par la propriété systeme "db_config".
     * Il suffit d'ajouter l'argument `-Ddb_config=chemin`
     * Plusieurs fichiers properties peuvent être spécifié en les séparant par le caractère ":"
     * @throws IOException lors d'une erreur de lecture du fichier de configuration
     * @throws ClassNotFoundException lorsque le driver est introuvable
     * @throws SQLException lors d'une erreur de requête SQL.
     */
    private static void init() throws IOException, ClassNotFoundException, SQLException
    {
        if (System.getProperties().containsKey("db_config"))
        {
            for (String path : System.getProperty("db_config").split(":"))
            {
                try (FileInputStream fis = new FileInputStream(System.getProperty("db_config")))
                {
                    Properties p = new Properties();
                    p.load(fis);
                    init(p);
                } catch (IOException | ClassNotFoundException e) {
                    throw e;
                }
            }
        }
    }

    /**
     * Cette méthode permet d'initier une connexion à la base de donnée.
     * @param connectionName, le nom de la connexion
     * @return un objet {@code Connection} qui représente la connexion à la base de données.
     * @throws IOException lors d'une erreur de lecture du fichier de configuration
     * @throws ClassNotFoundException lorsque le driver est introuvable
     * @throws SQLException lors d'une erreur de requête SQL.
     */
    public static Connection getConnection(String connectionName) throws SQLException, IOException, ClassNotFoundException
    {
        if (DBConnector.connectionDescriptorList == null)
        {
            init();
            if (DBConnector.connectionDescriptorList == null)
            {
                throw new SQLException("No connection initialized.");
            }
        }
        DBConnectionDescriptor currentConnectionDescriptor = DBConnector.connectionDescriptorList.get(connectionName == null ? DBConnector.defaultConnectionName : connectionName);
        if (currentConnectionDescriptor.getLogin() != null)
        {
            return DriverManager.getConnection(currentConnectionDescriptor.getUrl(), currentConnectionDescriptor.getLogin(), currentConnectionDescriptor.getPassword());
        }
        return DriverManager.getConnection(currentConnectionDescriptor.getUrl());
    }

    /**
     * Cette méthode permet d'initier une connexion à la base de donnée.
     * @return un objet {@code Connection}, qui représente la connexion à la base de données.
     * @throws IOException lors d'une erreur de lecture du fichier de configuration
     * @throws ClassNotFoundException lorsque le driver est introuvable
     * @throws SQLException lors d'une erreur de requête SQL.
     */
    public static Connection getConnection() throws SQLException, IOException, ClassNotFoundException
    {
        return DBConnector.getConnection(DBConnector.defaultConnectionName);
    }
}
