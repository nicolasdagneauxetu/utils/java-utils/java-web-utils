package fr.univlille.iut.info.webutils.model.data;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Cette classe permet de simplifier la communication avec la base de données, et d'éviter la redondance de code.
 * En particulier, elle permet de parcourir tous les résultats à travers un ResultSet
 * Le ResultSet est fourni à l'attribut {@param loop} de type {@code Loopable} passé en paramètre.
 * Ce dernier exécute du le même code pour chaque ligne.
 * @param <E>, le type de l'objet qui sera manipulé lors de la boucle
 */
public class DBLoop<E>
{
    /**
     * L'objet qui sera manipulé par l'objet de type {@code Loopable}
     */
    protected E obj;

    /**
     * La requête SQL à exécuter
     */
    String sqlRequest;

    /**
     * Le morceau de code qui sera exécuté à chaque ligne de résultat {@code Executable}.
     */
    protected Loopable<E> loop;

    /**
     * Constructeur
     * @param obj un objet de type E, qui sera manipulé par l'objet de type {@code Loopable}
     * @param sqlRequest la requête SQL à exécuter.
     * @param loop le morceau de code {@code Loopable} à exécuter à chaque ligne de résultat.
     */
    public DBLoop(E obj, String sqlRequest, Loopable<E> loop)
    {
        this.obj = obj;
        this.sqlRequest = sqlRequest;
        this.loop = loop;
    }

    /**
     * Cette méthode permet d'exécuter la requête {@code requete}.
     * @return l'objet qui a été transmis lors de l'instantiation.
     * @throws SQLException en cas d'erreur SQL, et ferme la connexion de manière automatique.
     * @throws IOException en cas d'erreur SQL, et ferme la connexion de manière automatique.
     * @throws ClassNotFoundException lorsque le driver est introuvable
     */
    public E executeLoop() throws SQLException, IOException, ClassNotFoundException {
        try (Connection c = DBConnector.getConnection())
        {
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(this.sqlRequest);
            while (rs.next())
            {
                loop.doLoop(this.obj, rs);
            }
            return this.obj;
        }
        catch (SQLException e)
        {
            throw e;
        }
    }
}
