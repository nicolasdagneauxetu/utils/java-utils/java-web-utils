package fr.univlille.iut.info.webutils.model.data;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Cette classe permet de simplifier la communication avec la base de données, et d'éviter la redondance de code.
 * La connexion est fourni à l'attribut {@param request} de type {@code Executable} passé en paramètre.
 * Ce dernier exécute du code et retourne un résultat de type E.
 * @param <E>, le type de retour attendu après exécution de la requête
 */
public class DBRequest<E>
{
    /**
     * Une requête {@code Executable}.
     */
    protected Executable<E> request;

    /**
     *
     * @param request, la requête {@code Executable} à exécuter.
     */
    public DBRequest(Executable<E> request)
    {
        this.request = request;
    }

    /**
     * Cette méthode permet d'exécuter la requête {@code requete}.
     * @return un résultat de type E.
     * @throws SQLException en cas d'erreur SQL, et ferme la connexion de manière automatique.
     * @throws IOException en cas d'erreur SQL, et ferme la connexion de manière automatique.
     * @throws ClassNotFoundException lorsque le driver est introuvable
     */
    public E executeRequest() throws SQLException, IOException, ClassNotFoundException {
        try (Connection c = DBConnector.getConnection())
        {
            return this.request.execute(c);
        }
        catch (SQLException e)
        {
            throw e;
        }
    }

    /**
     * Cette méthode permet d'exécuter la requête sans commit automatique de la transaction {@code requete}.
     * @param isolationLevel un {@code int} représentant le niveau d'isolation de la transaction.
     * @return un résultat de type E.
     * @throws SQLException en cas d'erreur SQL, ferme la connexion et réalise un rollback de manière automatique.
     * @throws IOException en cas d'erreur SQL, et ferme la connexion de manière automatique.
     * @throws ClassNotFoundException lorsque le driver est introuvable
     */
    public E executeTransaction(int isolationLevel) throws SQLException, IOException, ClassNotFoundException {
        Connection c = null;
        try
        {
            c = DBConnector.getConnection();
            c.setAutoCommit(false);
            c.setTransactionIsolation(isolationLevel);
            E resultat = this.request.execute(c);
            c.commit();
            c.close();
            return resultat;
        }
        catch (SQLException e)
        {
            try {
                c.rollback();
                c.close();
            } catch (SQLException exc) {
                c.close();
                throw exc;
            }
        }
        return null;
    }
}
