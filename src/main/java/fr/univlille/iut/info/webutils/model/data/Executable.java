package fr.univlille.iut.info.webutils.model.data;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Cette interface définit ce qu'est une requête exécutable.
 * @param <E> le type de retour
 */
public interface Executable<E>
{
    /**
     * Permet d'exécuter une requête SQL.
     * @param c, la {@code Connection} à la base de données
     * @return un résultat de type E.
     * @throws SQLException en cas d'erreur SQL.
     */
    public E execute(Connection c) throws SQLException;
}