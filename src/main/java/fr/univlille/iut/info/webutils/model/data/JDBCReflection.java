package fr.univlille.iut.info.webutils.model.data;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;

class JDBCReflection
{
    private Method sqlMethod;
    private String colName;

    public JDBCReflection(Method sqlMethod, String colName)
    {
        this.sqlMethod = sqlMethod;
        this.colName = colName;
    }

    public Method getSqlMethod()
    {
        return sqlMethod;
    }

    public Object execute(ResultSet rs) throws InvocationTargetException, IllegalAccessException
    {
        return this.sqlMethod.invoke(rs, this.colName);
    }
}
