package fr.univlille.iut.info.webutils.model.data;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Cette interface définit ce qu'est une fonction qui peut être appliqué à chaque ligne de résultat de requête sql.
 * @param <E> le type de retour
 */
public interface Loopable<E>
{
    /**
     * Permet d'exécuter du code sur une seule ligne de résultat de requête SQL.
     * @param obj l'objet de type E qui sera manipulé.
     * @param rs la ligne de résultat en cours de traitement
     * @throws SQLException en cas d'erreur SQL.
     */
    public void doLoop(E obj, ResultSet rs) throws SQLException;
}
