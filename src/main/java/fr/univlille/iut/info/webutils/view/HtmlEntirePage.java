package fr.univlille.iut.info.webutils.view;

/**
 * Cette classe définit ce qu'est une page HTML entière (head et body)
 */
public abstract class HtmlEntirePage
{
    protected View[] viewList;
    protected String title;

    /**
     * @param title le tire de la page
     * @param views toutes les vues à intégrer dans la balise body de la page
     */
    public HtmlEntirePage(String title, View... views)
    {
        this.title = title;
        this.viewList = views;
    }

    /**
     * Cette méthode permet de définir le contenu de la balise head de la page
     * @return l'entièreté de la partie head de la page html
     */
    protected abstract StringBuilder getHead();

    /**
     * Cette méthode permet de définir le contenu de la balise body de la page
     * @return l'entièreté de la partie body de la page html
     */
    public final StringBuilder getBody()
    {
        StringBuilder body = new StringBuilder("<body>");
        for (View currentView : viewList)
        {
            body.append(currentView.getContent());
        }
        body.append("</body>");
        return body;
    }

    /**
     * Cette méthode permet d'obtenir l'entièreté de la page html'
     * @return l'entièreté de la page html
     */
    public final StringBuilder getEntirePage()
    {
        StringBuilder entirePage = new StringBuilder("<?xml version='1.0' encoding='UTF-8'?>");
        entirePage.append("<!DOCTYPE html>");
        entirePage.append("<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='fr' lang='fr' dir='ltr'>");
        entirePage.append(this.getHead());
        entirePage.append(this.getBody());
        entirePage.append("</html>");
        return entirePage;
    }
}