package fr.univlille.iut.info.webutils.view;

/**
 * Cette interface définit ce qu'est une vue : Selon des paramètres passés lors de l'instanciation, un contenu (html ou autre) est retourné.
 *
 */
public interface View
{
    /**
     * @return le contenu de la vue à afficher
     */
    public StringBuilder getContent();
}