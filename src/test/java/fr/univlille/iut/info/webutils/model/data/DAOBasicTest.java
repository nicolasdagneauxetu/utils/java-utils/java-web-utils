package fr.univlille.iut.info.webutils.model.data;

import java.util.List;

public class DAOBasicTest
{
    public static void main(String[] args) throws Exception
    {
        Class.forName("org.postgresql.Driver");
        DBConnector.init(new DBConnectionDescriptor(
                "org.postgresql.Driver",
                "jdbc:postgresql://localhost:5432/but2",
                "nicolasdagneauxetu",
                "moi"));

        EtudiantDAO dao = new EtudiantDAO();

        List<Etudiant> etus = dao.findAll();
        System.out.println(etus);
    }
}
