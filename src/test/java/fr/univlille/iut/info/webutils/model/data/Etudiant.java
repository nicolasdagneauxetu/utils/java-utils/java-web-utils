package fr.univlille.iut.info.webutils.model.data;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public record Etudiant(String nom, String prenom)
{
}
