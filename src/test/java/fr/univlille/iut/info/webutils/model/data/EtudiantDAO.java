package fr.univlille.iut.info.webutils.model.data;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class EtudiantDAO extends DAOBasic<Etudiant>
{
    public EtudiantDAO() throws NoSuchMethodException
    {
        super(Etudiant.class);
    }

    public List<Etudiant> findAll() throws SQLException, IOException, ClassNotFoundException
    {
        return super.findAll();
    }
}
